package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class OstasolgTest {

    @Test
    public void factorialTest1() {

        Ostasolg fact = new Ostasolg();

        assertEquals(1, fact.factorial(0));
        assertEquals(1, fact.factorial(1));
        assertEquals(2, fact.factorial(2));
        assertEquals(6, fact.factorial(3));
        assertEquals(24, fact.factorial(4));
        assertEquals(120, fact.factorial(5));

    }

    @Test
    public void factorialTest2() {

        Ostasolg fact = new Ostasolg();

        assertTrue(24 == fact.factorial(4));

    }

}
